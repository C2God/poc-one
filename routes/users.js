const userRoutes = (app, fs) => {
  // loading users.json path
  const dataPath = "./data/users.json";

  // refactored helper methods
  //reading file method
  const readFile = (
    callback,
    returnJson = false,
    filePath = dataPath,
    encoding = "utf8"
  ) => {
    fs.readFile(filePath, encoding, (err, data) => {
      if (err) {
        throw err;
      }

      callback(returnJson ? JSON.parse(data) : data);
    });
  };
  //write file method
  const writeFile = (
    fileData,
    callback,
    filePath = dataPath,
    encoding = "utf8"
  ) => {
    fs.writeFile(filePath, fileData, encoding, (err) => {
      if (err) {
        throw err;
      }

      callback();
    });
  };

  // GET ALL USERS
  app.get("/users", (req, res) => {
    //reading users data from users.json file
    readFile((data) => {
      //sending response to client
      res.send(data);
    }, true);
  });
  //GET SINGLE USER
  app.get("/users/:id", (req, res) => {
    //reading data from users.json file
    readFile((data) => {
      //getting userId from request data
      const userId = req.params["id"];
      //sending the user according to userId to client
      res.send(data[userId]);
    }, true);
  });
  // CREATE NEW USER
  app.post("/users", (req, res) => {
    readFile((data) => {
      // Note: this is auto increment id which is taken from
      // total count of data adding +1 to it
      const currentId = Object.keys(data)?.length;
      const newUserId = (currentId + 1).toString();

      // add the new user
      data[newUserId] = req.body;
      //Writing new user to users.json
      writeFile(JSON.stringify(data, null, 2), () => {
        //sending response to client
        res.status(200).send("new user added");
      });
    }, true);
  });
  // UPDATE USER
  app.put("/users/:id", (req, res) => {
    readFile((data) => {
      //getting userId from request data
      const userId = req.params["id"];
      //updating user data
      data[userId] = req.body;

      // updating the new user to users.json file
      writeFile(JSON.stringify(data, null, 2), () => {
        //sending response to client
        res.status(200).send(`users id:${userId} updated`);
      });
    }, true);
  });
  // DELETE USER
  app.delete("/users/:id", (req, res) => {
    readFile((data) => {
      //getting userId from request data
      const userId = req.params["id"];
      //delete user from data list
      delete data[userId];
      //updating new data to users.json file
      writeFile(JSON.stringify(data, null, 2), () => {
        //sending response to client
        res.status(200).send(`users id:${userId} removed`);
      });
    }, true);
  });
};

module.exports = userRoutes;
