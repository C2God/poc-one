// loading new route for users
const userRoutes = require("./users");

const appRouter = (app, fs) => {
  // default route here that handles empty routes
  // at the base API url
  app.get("/", (req, res) => {
    res.send("welcome to the development api-server");
  });

  // run our user route module here to complete the wire up
  userRoutes(app, fs);
};

// exporting the appRouter
module.exports = appRouter;
