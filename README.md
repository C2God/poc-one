### Getting started

Run the below command to install dependencies:

```
yarn install

// or

npm install
```
Run below command To fire up the server:

```
yarn start

// or

npm start
```

### Accessing the server and returning data

The server should be running by now, and you can visit `http://localhost:3001` to see it in action. 

`http://localhost:3001/users` -- will use a GET request to API server and gives the data as json response.

To access the POST, PUT and DELETE you need to use tool like `POST MAN`

Method: `POST`
API: `http://localhost:3001/users`
RequestBody: 
```
{   
    "name": "Test", 
    "email": "test@test.com",
    "profession": "Tester"
}
```
Response: `new user added`.

Description: This api is used to add new user, using the above request body in `POST MAN`.

Method: `PUT`
API: `http://localhost:3001/users/:id`.

RequestBody: 
```
{   
    "name": "Test", 
    "email": "test@test.com",
    "profession": "Tester"
}
```
Response: `users id:5 updated`.

Description: This api is used to update the user, using the above request body in `POST MAN`.

Method: `DELETE`.

API: `http://localhost:3001/users/:id`.

Response: `users id:5 removed`.

Description: This api is used to delete the user.